#include <stdlib.h> /* exit(), malloc(), free() */
#include <stdio.h> /* printf(), fprintf(), perror() */
#include <unistd.h> /* fork(), close() */
#include <sys/wait.h> /* waitpid() */
#include <sys/types.h>
#include <sys/socket.h> /* socket(), accept(), bind(), listen(),
						   struct addrinfo, sockaddr */
#include <netdb.h> /* getaddrinfo(), freeaddrinfo(), gai_strerror(),
					  struct addrinfo */
#include <netinet/in.h> /* struct sockaddr_in, in_addr */
#include <arpa/inet.h> /* inet_ntop() */
#include <string.h> /* memset() */
#include <errno.h> /* errno */
#include <signal.h> /* struct sigaction, sigaction(), sigemptyset() */

#include "keyvalue.h"

const char *PORT = "3245";
const int BUF_SIZE = 1024;
const char *PUT_ERR = "Error: Command format is PUT <KEY> <VALUE>\n";
const char *GET_ERR = "Error: Command format is GET <KEY>\n";
const char *DEL_ERR = "Error: Command format is DEL <KEY>\n";
const char *SYNTAX_ERR = "Syntax is <COMMAND> <KEY> [<VALUE>]\n";

void sigchld_handler(int s);
struct addrinfo* get_addr_list(const char *port);
int prep_socket(struct addrinfo *list, int yes);

int main(int argc, char **argv)
{
	int sockfd, client_fd;
	struct addrinfo *serv_info;
	struct sockaddr_storage client_addr;
	socklen_t sin_size;
	struct sigaction sa;
	int yes = 1;
	int recvd;
	char recv_buffer[BUF_SIZE];
	char *command;
	kvNode *arrkeys[arraylen] = {NULL};
	keys = &arrkeys[0];

	serv_info = get_addr_list(PORT);

	if((sockfd = prep_socket(serv_info, yes)) == -1) {
		exit(EXIT_FAILURE);
	}

	if(listen(sockfd, 1) == -1) {
		perror("listen");
		exit(EXIT_FAILURE);
	}

	sa.sa_handler = sigchld_handler;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;

	if(sigaction(SIGCHLD, &sa, NULL) == -1) {
		perror("sigaction");
		exit(EXIT_FAILURE);
	}

	printf("server: Ready for connections...\n");

	while(1) {
		sin_size = sizeof client_addr;
		client_fd = accept(sockfd, (struct sockaddr *)&client_addr, &sin_size);
		if(client_fd == -1) {
			perror("accept");
			continue;
		}

		/*if((recvd = recv(client_fd, recv_buffer, BUF_SIZE, 0)) != 0) {*/
		while((recvd = recv(client_fd, recv_buffer, BUF_SIZE, 0)) != 0) {
			if(recvd > 0) {
				char *p_operation = recv_buffer;
				char *next_token = NULL;
				command = kv_strtok(p_operation, " \n", &next_token);

				strUpper(command);

				if(command == NULL) {
					continue;
				}

				if(strcmp("PUT", command) == 0) {
					char *key = kv_strtok(NULL, " \n", &next_token);
					char *value = kv_strtok(NULL, " \n", &next_token);

					if(!key || !value) {
						if(send(client_fd, PUT_ERR,
									strlen(PUT_ERR), 0) == -1) {
							perror("send");
						}
					}

					else {
						kvArgs *kvargs = (kvArgs *) malloc(sizeof(kvArgs));
						kvargs->cKey = key;
						kvargs->iValue = strtol(value, NULL, 0);

						kv_new_thread(put, kvargs);
					}
				}

				else if(strcmp("GET", command) == 0) {
					char *key = kv_strtok(NULL, " \n", &next_token);

					if(!key) {
						if(send(client_fd, GET_ERR,
									strlen(GET_ERR), 0) == -1) {
							perror("send");
						}
					}

					else {
						kvArgs *kvargs = (kvArgs *) malloc(sizeof(kvArgs));
						kvargs->cKey = key;

						kv_new_thread(get, kvargs);
					}
				}

				else if(strcmp("DEL", command) == 0) {
					char *key = kv_strtok(NULL, " \n", &next_token);

					if(!key) {
						if(send(client_fd, DEL_ERR,
									strlen(DEL_ERR), 0) == -1) {
							perror("send");
						}
					}

					else {
						kvArgs *kvargs = (kvArgs *) malloc(sizeof (kvArgs));
						kvargs->cKey = key;

						kv_new_thread(del, kvargs);
					}
				}

				else {
					if(strcmp("EXIT", command) != 0)
						if(send(client_fd, SYNTAX_ERR,
									strlen(SYNTAX_ERR), 0) == -1) {
							perror("send");
						}
				}
			}

			else {
				fprintf(stderr, "Problem receiveing data\n");
				continue;
			}
		}

		/*else {*/
			/*fprintf(stderr, "Client closed connection\n");*/
		/*}*/

		close(client_fd);
	}

	close(sockfd);

	return 0;
}


void sigchld_handler(int s)
{
	int saved_errno = errno;

	while(waitpid(-1, NULL, WNOHANG) > 0);

	errno = saved_errno;
}

struct addrinfo* get_addr_list(const char *port)
{
	int status;
	struct addrinfo hints, *serv; 

	memset(&hints, 0, sizeof hints);
	hints.ai_flags = AI_PASSIVE;
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;

	if((status = getaddrinfo(NULL, port, &hints, &serv)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(status));
		exit(EXIT_FAILURE);
	}

	return serv;
}

int prep_socket(struct addrinfo *list, int yes)
{
	struct addrinfo *p;
	int sock;

	for(p = list; p != NULL; p = p->ai_next) {
		if((sock = socket(p->ai_family, p->ai_socktype,
						p->ai_protocol)) == -1) {
			perror("server: socket");
			continue;
		}

		if(setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &yes,
					sizeof(int)) == -1) {
			perror("setsockopt");
			exit(EXIT_FAILURE);
		}

		if(bind(sock, p->ai_addr, p->ai_addrlen) == -1) {
			close(sock);
			perror("server: bind");
			continue;
		}

		break;
	}

	freeaddrinfo(list);

	return sock;
}

void put(void* args)
{
	char* key = ((kvArgs*)args)->cKey;
	int value = ((kvArgs*)args)->iValue;

	//Grab hash and mod it w/ array length
	uint32_t index = hashkey(key) % arraylen;

	//Create new node and store key and value
	kvNode* NewNode = (kvNode*)malloc(sizeof(kvNode));

	//Copy up to first 32 bytes of key into array
	for (int i = 0; (i < 32 || *(key + i) != '\0'); i++)
	{
		NewNode->cKey[i] = *(key + i);
	}

	//Copy Value and assign NEXT pointer to NULL;
	NewNode->iValue = value;
	NewNode->NEXT = NULL;

	kvNode **node;
	node = &keys[index];

	while (*node)
	{
		if (strcmp((*node)->cKey, key)==0)
		{
		    (*node)->iValue = value;

		    free(args);
		    return;
		}

		node = &((*node)->NEXT);
	}

	(*node) = NewNode;

	free(args);
	return;

}

//Returns value given the key
void get(void* args)
{
	char *key = ((kvArgs*)args)->cKey;

	//Grab hash and mod it w/ array length
	uint32_t index = hashkey(key) % arraylen;

	//Iterate through list and find the key
	kvNode *node = keys[index];

	while (node)
	{
		if (strcmp(node->cKey, key) == 0)
		{
            printf("Value of key is: %u\n", node->iValue);
			free(args);
			return;
		}

		else
			node = node->NEXT;
	}

	printf("key <%s> is not in store\n", key);
	free(args);
	return;
}

//Deletes a key value pair from the store
void del(void* args)
{
	char *key = ((kvArgs*)args)->cKey;

	//Grab hash and mod it w/ array length
	uint32_t index = hashkey(key) % arraylen;

	//Iterate through list and try to find the key
	kvNode **node = &keys[index];
	kvNode *prevNode = NULL; //pointer to previous node in list

	while (*node)
	{
		if (strcmp((*node)->cKey,key)==0)
		{ //node to be deleted has been found

			//Make sure list isn't broken if deletion node isn't last in the list
			if ((*node)->NEXT)
			{
				kvNode *delNode = prevNode->NEXT; //Node to be deleted
				prevNode->NEXT = (*node)->NEXT;
				free(delNode);
				free(args);
				return;
			}

			else
			{
				free(*node);
				free(args);
				return;
			}
		}
		else
			prevNode = *node;
			node = &((*node)->NEXT);
	}

	free(args);
	return;
}

uint32_t hashkey(char *key)
{

	uint32_t hashval = 0; //our hash
	int i = 0;

	/* Convert our string to an integer */
	while (hashval < ULONG_MAX && i < strlen(key)) {
		hashval = hashval << 8;
		hashval += key[i];
		i++;
	}

	return hashval;
}

//Utility function to transform string to upper case char array
void strUpper(char *c)
{
	if(c == NULL)
		return;

	for (int i = 0; c[i] != '\0'; i++)
	{
		c[i] = (char)toupper(c[i]);
	}
}
