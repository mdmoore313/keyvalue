
ifeq ($(OS),Windows_NT)
    SOURCES += winkeyvalue.c
else
    UNAME_S := $(shell uname -s)
    ifeq ($(UNAME_S),Linux)
	SOURCES += unixkeyvalue.c
	CFLAGS += -pthread
    endif
endif


CFLAGS+=-Wall -g -std=gnu99 -c
CC=gcc
LDFLAGS= -pthread
SOURCES+=keyvalue.c server2.c keyvalue.c kvlirs.c
OBJECTS=$(SOURCES:.c=.o)
EXECUTABLE=keyvalue

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.c.o:
	$(CC) $(CFLAGS) $< -o $@
