#include <stdlib.h> /* exit(), malloc(), free() */
#include <stdio.h> /* printf(), fprintf(), perror() */
#include <unistd.h> /* fork(), close() */
#include <sys/types.h>
#include <sys/socket.h> /* socket(), accept(), bind(), listen(),
						   struct sockaddr */
#include <netdb.h> /* getaddrinfo(), freeaddrinfo(), gai_strerror(),
					  struct addrinfo */
#include <netinet/in.h> /* struct sockaddr_in, in_addr */
#include <arpa/inet.h> /* inet_ntop() */
#include <string.h> /* memset() */
#include <errno.h> /* errno */

#include "keyvalue.h"

const int BUF_SIZE = 100;
const char *PORT = "3245";
struct addrinfo* get_addr_list(const char *port, char *host);
int prep_socket(struct addrinfo *list);
void* get_in_addr(struct sockaddr *sa);

int main(int argc, char **argv)
{
	int sockfd;
	char recvd_buf[BUF_SIZE];
	struct addrinfo *servinfo;
	char operation[32];
	char p_op[32];
	char *command = NULL;

	if(argc != 2) {
		fprintf(stderr, "Usage: kv-cli <hostname>\n");
		exit(EXIT_FAILURE);
	}

	servinfo = get_addr_list(PORT, argv[1]);

	if((sockfd = prep_socket(servinfo)) == -1) {
		fprintf(stderr, "client: failed to connect\n");
		exit(EXIT_FAILURE);
	}

	/*TODO: Implement client-side shell here */
	printf("Type 'exit' to end\n");

	while((!command) || strcmp("exit", command) != 0) {
		printf("kv-cli> ");
		fgets(operation, 100, stdin);
		strcpy(p_op, operation);

		command = strtok(p_op, " \n");

		if(send(sockfd, operation, strlen(operation), 0) == -1) {
			perror("send");
			continue;
		}

		if(recv(sockfd, recvd_buf, BUF_SIZE - 1, 0) == -1) {
			perror("recv");
			continue;
		}

		printf("%s\n", recvd_buf);
	}
	
	close(sockfd);

	printf("Connection closed\n");

	return 0;
}


struct addrinfo* get_addr_list(const char *port, char *host)
{
	int rv;
	struct addrinfo hints, *serv;

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;

	if((rv = getaddrinfo(host, port, &hints, &serv)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		exit(EXIT_FAILURE);
	}

	return serv;
}


int prep_socket(struct addrinfo *list)
{
	struct addrinfo *p;
	int sock;
	char s[INET6_ADDRSTRLEN];

	for(p = list; p != NULL; p = p->ai_next) {
		if((sock = socket(p->ai_family, p->ai_socktype,
						p->ai_protocol)) == -1) {
			perror("client: socket");
			continue;
		}

		if(connect(sock, p->ai_addr, p->ai_addrlen) == -1) {
			close(sock);
			perror("client: connect");
			continue;
		}

		break;
	}

	if(p != NULL) {
		inet_ntop(p->ai_family, get_in_addr((struct sockaddr *)p->ai_addr),
				s, sizeof s);
		printf("client: connecting to %s\n", s);

		freeaddrinfo(list);

		return sock;
	}

	freeaddrinfo(list);

	return -1;
}


void* get_in_addr(struct sockaddr *sa)
{
	if(sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in *)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6 *)sa)->sin6_addr);
}
