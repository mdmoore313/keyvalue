// keyValue.cpp : Defines the entry point for the console application.
//TODO: MULTITHREADING AND LINK LOCK
//TODO: NETWORK INTERFACE

#include "keyvalue.h"



int main()
{
	//Initialize lirstacks
	kvlirstack = (LIRStack*)malloc(sizeof(LIRStack));
	kvhirstack = (LIRStack*)malloc(sizeof(LIRStack));
	LIRSinitstack(kvlirstack, lirlength);
	LIRSinitstack(kvhirstack, hirlength);
	
	//Create array of pointers to kvNodes
	kvNode *arkeys[arraylen] = { NULL };
        keys = &arkeys[0];

	//Storing user input
	char operation[32];

	//Pointer to user's command
	char *command = NULL;

	while ((!command)|| strcmp("EXIT", command) != 0)
	{
		//Grab operation from command line and process
		printf("Enter operation (type 'exit' to end): ");
		fgets(operation, 100, stdin);

		//Parse data and perform operation

		//Pointer to command string
		char *pOperation = operation;
		char *next_token = NULL;

		//Grab the actual Command from the string
                command = kv_strtok(pOperation, " \n", &next_token);

		//Transform to upper case for switch case
		strUpper(command);

		if (command == NULL)
			continue;

		//determine command and parse accordingly
		if (strcmp("PUT", command) == 0)
		{
			//parse key and value and add accordingly
                        char *key = kv_strtok(NULL, " \n", &next_token);
                        char *value = kv_strtok(NULL, " \n", &next_token);

			//Check if both key and value are present on command line
			if (!key || !value)
			{
				printf("Error: Format of command is PUT <KEY> <VALUE>\n");
			}


			else
			{
			    kvArgs* kvargs = (kvArgs*)malloc(sizeof(kvArgs));
			    kvargs->cKey = key;
			    kvargs->iValue = strtol(value, NULL, 0);

			    kv_new_thread(put, kvargs);
			}
		}

		else if (strcmp("GET", command) == 0)
		{
			//parse key
                        char *key = kv_strtok(NULL, " \n", &next_token);

			if (!key)
			{
				printf("Error: Format of command is GET <KEY>\n");
			}

			else
			{
			    kvArgs* kvargs = (kvArgs*)malloc(sizeof(kvArgs));
			    kvargs->cKey = key;

			    kv_new_thread(get, kvargs);
			}

		}//end GET

		else if (strcmp("DEL", command) == 0)
		{
			//parse key
                        char *key = kv_strtok(NULL, " \n", &next_token);

			if (!key)
			{
				printf("Error: Format of command is DEL <KEY>\n");
			}

			else
			{
			    kvArgs* kvargs = (kvArgs*)malloc(sizeof(kvArgs));
			    kvargs->cKey = key;

			    kv_new_thread(del, kvargs);
			}
		}//end DEL

		else
			if (strcmp("EXIT", command) != 0)
				fprintf(stderr, "Syntax is <COMMAND> <KEY> [<VALUE>]\n");
	} //end user input
    return 0;
}

//hash key and store in index position of hash modded w/ array size
void put(void* args)
{
	char* key = ((kvArgs*)args)->cKey;
	int value = ((kvArgs*)args)->iValue;

	//Grab hash and mod it w/ array length
	uint32_t index = hashkey(key) % arraylen;
	
	//Create new node and store key and value
	kvNode* NewNode = (kvNode*)malloc(sizeof(kvNode));
	
	//Copy up to first 32 bytes of key into array
	for (int i = 0; (i < 32 || *(key + i) != '\0'); i++)
	{
		NewNode->cKey[i] = *(key + i);
	}

	//Copy Value and assign NEXT pointer to NULL;
	NewNode->iValue = value;
	NewNode->NEXT = NULL;
	
        //Initialize LIRinfo stack and add pointer to kvnode
	NewNode->lirinfo = (LIRNode*)malloc(sizeof(LIRNode));

	NewNode->lirinfo->lirstackinfo = (LIRNodeStackInfo*)malloc(sizeof(LIRNodeStackInfo));
	NewNode->lirinfo->hirstackinfo = (LIRNodeStackInfo*)malloc(sizeof(LIRNodeStackInfo));

        NewNode->lirinfo->lirstackinfo->kvnode=NewNode;
        NewNode->lirinfo->hirstackinfo->kvnode=NewNode;

	kvNode **node;
	node = &keys[index];

	while (*node)
	{
		if (strcmp((*node)->cKey, key)==0)
		{
		    (*node)->iValue = value;
			//An existing node was updated, a new node isn't needed so 
			//let's free the one we malloc'd
			free(NewNode);
		    free(args);
			
			//Update LIRS
			LIRSrefresh(kvlirstack, kvhirstack, (*node)->lirinfo);
		    return;
		}

		node = &((*node)->NEXT);
	}

	(*node) = NewNode;
	
	//Update LIRS
	LIRSrefresh(kvlirstack, kvhirstack, (*node)->lirinfo);
	free(args);
	return;

}

//Returns value given the key
void get(void* args)
{
	char *key = ((kvArgs*)args)->cKey;

	//Grab hash and mod it w/ array length
	uint32_t index = hashkey(key) % arraylen;

	//Iterate through list and find the key
	kvNode *node = keys[index];
	
	while (node)
	{
		if (strcmp(node->cKey, key) == 0)
		{
			printf("Value of key is: %u\n", node->iValue);
			free(args);
			
			//Update LIRS
			LIRSrefresh(kvlirstack, kvhirstack, node->lirinfo);
			return;
		}

		else
			node = node->NEXT;
	}
	
	printf("key <%s> is not in store\n", key);
	
	//If we are monitoring the performance of LIRS, increase miss count
	#ifdef LIRSPERFMEASURE
	//increase hit or miss count depending on status
	LIRSperfmeasure(false); 
	#endif
	
	free(args);
	return;	
}

//Deletes a key value pair from the store
void del(void* args)
{
	char *key = ((kvArgs*)args)->cKey;

	//Grab hash and mod it w/ array length
	uint32_t index = hashkey(key) % arraylen;

	//Iterate through list and try to find the key
	kvNode **node = &keys[index];
	kvNode *prevNode = NULL; //pointer to previous node in list

	while (*node)
	{
		if (strcmp((*node)->cKey,key)==0)
		{ //node to be deleted has been found
			
                        //Remove node from HIRStack
                        //Flag node for removal from LIRStack
                        (*node)->lirinfo->lirstackinfo->status = STATUS_NHIR;
                        LIRSremovenode(kvhirstack, (*node)->lirinfo->hirstackinfo);
			
			//Handle deleting first item in list differently
			if(!prevNode)
			{				
				//Grab pointer to next node
				kvNode* oldFirstNode = *node;
				*node = oldFirstNode->NEXT;
				//Delete node
				free(oldFirstNode);
				free(args);
				return;
			}
			
			//Handle a node in the middle of the list
			if ((*node)->NEXT)
			{
				kvNode *delNode = prevNode->NEXT; //Node to be deleted
				prevNode->NEXT = (*node)->NEXT;
				free(delNode);
				free(args);
				return;
			}

			//Last item in list. Delete and return
			else
			{
				free(*node);
				free(args);
				return;
			}
		}//End found node
		
		//Iterate and check again
		else
			prevNode = *node;
			node = &((*node)->NEXT);
	}

	free(args);
	return;
}

uint32_t hashkey(char *key)
{

	uint32_t hashval = 0; //our hash
	int i = 0;

	/* Convert our string to an integer */
	while (hashval < ULONG_MAX && i < strlen(key)) {
		hashval = hashval << 8;
		hashval += key[i];
		i++;
	}

	return hashval;
}

//Utility function to transform string to upper case char array
void strUpper(char *c)
{
	if(c == NULL)
		return;

	for (int i = 0; c[i] != '\0'; i++)
	{
		c[i] = (char)toupper(c[i]);
	}
}
