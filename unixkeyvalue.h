#pragma once

#include "keyvalue.h"
#include <string.h>

char* unixkv_strtok(char*,const char*, char**); //Wrapper for strtok_r
void* unixkv_new_thread(void (*thread_main)(void*), void*); //Wrapper for new thread function
