#pragma once
#include <ctype.h>
#include <inttypes.h>
#include <limits.h>
#include <stdarg.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "kvlirs.h"

//Include system specific function headers if necessary
#if defined(_WIN32)
#include "winkeyvalue.h"
#endif
#if !defined(_WIN32) && (defined(__unix__) || defined(__unix) || (defined(__APPLE__) && defined(__MACH__)))
/* UNIX-style OS. ------------------------------------------- */
#include "unixkeyvalue.h"
#endif

#define arraylen 10
#define hirlength 3
#define lirlength 10

typedef struct kvNode kvNode;
typedef struct kvArgs kvArgs;

//Pointers to OS specific functions
typedef void* (*os_kv_new_thread)(void(*kv_new_thread)(void*), void*); //Create new thread
typedef char* (*os_kv_strtok)(char*, const char*, char**); //String token function

os_kv_strtok kv_strtok;
os_kv_new_thread kv_new_thread;


//Function declarations
void put(void*); //Insert a key value pair
void get(void*); //Retrieve the value for a given key
void del(void*); //Remove a key value pair

uint32_t hashkey(char *key); //Utility function; Generic hashing function
void strUpper(char *); //Utility function; converts string to upper case

//Variable declarations
kvNode **keys; //pointer to our key array
struct LIRStack* kvlirstack; //cache replacment stacks
struct LIRStack* kvhirstack;

struct kvNode
{ char cKey[32];
  uint32_t iValue;
  struct kvNode *NEXT;
  struct LIRNode* lirinfo;
};

struct kvArgs
{
    char* cKey;
    uint32_t iValue;
};


