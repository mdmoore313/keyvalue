#pragma once

//LIRS cache replacement algorithm implementation on keyvalue cache

#include "keyvalue.h"
#include <stdbool.h>

//Hit and miss counters used during performance tracking
uint32_t hit;
uint32_t miss;


typedef enum LIRStatus {STATUS_LIR, STATUS_HIR, STATUS_NHIR} LIRStatus;
typedef enum enumLIRStack {STACK_LIR, STACK_HIR} enumLIRStack;

typedef struct LIRNode LIRNode;
typedef struct LIRNodeStackInfo LIRNodeStackInfo;
typedef struct LIRStack LIRStack;

LIRStack lirstack;
LIRStack hirstack;

void LIRSrefresh(LIRStack*, LIRStack*, LIRNode*);
void LIRSprune(LIRStack*);
void LIRSnewnode(LIRStack*, LIRStack*, LIRNode*);
void LIRSremovenode(LIRStack*, LIRNodeStackInfo*);
void LIRSpushtop(LIRStack*, LIRNodeStackInfo*);
void LIRSpopbottom(LIRStack*);
void LIRSperfmeasure(bool);
bool LIRSnodeinstack(LIRNodeStackInfo*);
void LIRSinitstack(LIRStack*, uint32_t);
void LIRSinitnode(LIRNode*);

struct LIRNode
{
	LIRNodeStackInfo* lirstackinfo;
	LIRNodeStackInfo* hirstackinfo;
};

struct LIRNodeStackInfo
{
        struct kvNode* kvnode;
	LIRStatus status;
	LIRNodeStackInfo* prev;
	LIRNodeStackInfo* next;
};

struct LIRStack
{
        LIRNodeStackInfo* top;
        LIRNodeStackInfo* bottom;
	uint32_t maxsize;
	uint32_t currentsize;
};

