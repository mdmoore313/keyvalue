#ifndef KV_SERV_H
#define KV_SERV_H
#include <sys/wait.h> /* waitpid() */
#include <signal.h> /* struct sigaction, sigaction(), sigemptyset() */
#include <stdbool.h> /* bool */

#include "kv.h"

typedef struct kv_node kv_node;

#ifdef CLOCK_ALG
typedef struct Clock Clock;
typedef struct ClockNode ClockNode;

struct Clock {
	ClockNode *top;
	ClockNode *bottom;
	uint32_t current_length;
	uint32_t max_length;
	ClockNode *current_position;
};

struct ClockNode {
	ClockNode *next;
	ClockNode *previous;
    struct kv_node* kvnode;
	bool reference; //set it true if needs to be reference
};

void Clock_Refresh(Clock*, ClockNode*);
void clockinit(Clock*, uint32_t);

// Cache replacement structure
Clock *kvclock;

#elif defined LIRS_ALG
#define hirlength 3
#define lirlength 10

typedef enum LIRStatus {STATUS_LIR, STATUS_HIR, STATUS_NHIR} LIRStatus;
typedef enum enumLIRStack {STACK_LIR, STACK_HIR} enumLIRStack;

typedef struct LIRNode LIRNode;
typedef struct LIRNodeStackInfo LIRNodeStackInfo;
typedef struct LIRStack LIRStack;


void LIRSrefresh(LIRStack*, LIRStack*, LIRNode*);
void LIRSprune(LIRStack*);
void LIRSnewnode(LIRStack*, LIRStack*, LIRNode*);
void LIRSremovenode(LIRStack*, LIRNodeStackInfo*);
void LIRSpushtop(LIRStack*, LIRNodeStackInfo*);
void LIRSpopbottom(LIRStack*);
void LIRSperfmeasure(bool);
bool LIRSnodeinstack(LIRNodeStackInfo*);
void LIRSinitstack(LIRStack*, uint32_t);
void LIRSinitnode(LIRNode*);

struct LIRNode {
	LIRNodeStackInfo* lirstackinfo;
	LIRNodeStackInfo* hirstackinfo;
};

struct LIRNodeStackInfo {
    struct kv_node* kvnode;
	LIRStatus status;
	LIRNodeStackInfo* prev;
	LIRNodeStackInfo* next;
};

struct LIRStack {
    LIRNodeStackInfo* top;
    LIRNodeStackInfo* bottom;
	uint32_t maxsize;
	uint32_t currentsize;
};

LIRStack *kvlirstack;
LIRStack *kvhirstack;

#else
typedef struct LRU LRU;
typedef struct LRUNode LRUNode;

struct LRU {
	LRUNode *top;
	LRUNode *bottom;
	uint32_t current_length;
	uint32_t max_length;
	LRUNode *current_position;
};

struct LRUNode {
	LRUNode *next;
	LRUNode *previous;
	struct kv_node* kvnode;
};

void LRU_Add(LRU*, LRUNode*);
void lruinit(LRU*, uint32_t);
void LRU_push(LRU*, LRUNode*);
void LRU_pop(LRU*);
void LRU_Remove(LRU*, LRUNode*);

LRU *kvLRU;
#endif

// Function declarations
void handle_request(void*);
void put(void*); //Insert a key value pair
void get(void*); //Retrieve the value for a given key
void del(void*); //Remove a key value pair

// TODO: Evaluate the necessity of this function
void sigchld_handler(int s); //Handle forked processes

struct addrinfo* get_addr_list(const char *port); //Get list of interfaces
int prep_socket(struct addrinfo *list, int yes); //Get an open socket

struct kv_node {
	char c_key[KEYLEN];
	uint32_t i_value;
	struct kv_node *next;
#ifdef CLOCK_ALG
	ClockNode *clockinfo;
#elif defined LIRS_ALG
	LIRNode *lirinfo;
#else
	LRUNode *kvLRUnode;
#endif
};

// Variable declarations
// Pointer to our key array
kv_node **keys;
// Lock for key array, one lock per bucket
pthread_mutex_t kv_lock[ARRAYLEN];
// Lock for the cache structure
pthread_mutex_t cache_lock;

#endif
