#include "kvlirs.h"

void LIRSrefresh(LIRStack* stackLIR, LIRStack* stackHIR, LIRNode* lirnode)
{	
	//Our kvNode's information regarding the LIRStack and HIRStack
	LIRNodeStackInfo *lirinfo = lirnode->lirstackinfo;
	LIRNodeStackInfo *hirinfo = lirnode->hirstackinfo;
	
	//If we are monitoring the performance of LIRS, increase hit count
	#ifdef LIRSPERFMEASURE
	//increase hit or miss count depending on status
	LIRSperfmeasure(((
		LIRSnodeinstack(lirinfo)||
		LIRSnodeinstack(hirinfo))&&lirinfo->status!=NHIR) true : false); 
	#endif
	
	//Check if new node, a node that isn't in either stack
	if(!LIRSnodeinstack(lirinfo) && !LIRSnodeinstack(hirinfo))
	{
		//Add to LIRStack as LIRNode if LIRStack isn't full
		if(stackLIR->currentsize < stackLIR->maxsize)
		{
			lirinfo->status=STATUS_LIR;
                        LIRSpushtop(stackLIR, lirinfo);
		}
		
		//LIRStack is full, add as HIR
		else
		{
			lirinfo->status=STATUS_HIR;
                        LIRSpushtop(stackLIR, lirinfo);
			
			//Add to HIRStack as well
			hirinfo->status=STATUS_HIR;
                        LIRSpushtop(stackHIR, hirinfo);
		
		}
		return;
	} //End new node operation
	
	//Operation for a LIRnode that already exists in StackLIR
	if(lirinfo->status==STATUS_LIR)
	{
		//If node is already on top, we are done
                if(stackLIR->top==lirinfo)
			return;
			
		//If node was on bottom, stack will need pruning
                else if(stackLIR->bottom==lirinfo)
		{
			//Else, remove and place on top
                        LIRSremovenode(stackLIR, lirinfo);
                        LIRSpushtop(stackLIR, lirinfo);
			LIRSprune(stackLIR);
			return;
		}
		
		//Else, remove and place on top
		else
		{
                        LIRSremovenode(stackLIR, lirinfo);
                        LIRSpushtop(stackLIR, lirinfo);
			return;
		}
	}//End LIRNode operation
	
	
	//Operation for HIR node in LIRStack
	if(LIRSnodeinstack(lirinfo)&&lirinfo->status==STATUS_HIR)
	{
		//Promote to top of LIRStack as LIRNode, remove from HIRStack if necessary
		lirinfo->status=STATUS_LIR;
                LIRSremovenode(stackLIR, lirinfo);
                LIRSpushtop(stackLIR, lirinfo);
		if(LIRSnodeinstack(hirinfo))
                        LIRSremovenode(stackHIR, hirinfo);
		
		//Demote bottom node, move to stackHIR, and prune stackLIR
                stackLIR->bottom->status=STATUS_HIR;
                LIRSpushtop(stackHIR, hirinfo);
		LIRSprune(stackLIR);
		return;
	}//End HIR node in LIRStack operation
	
	//Operation for HIR node not in LIRStack
	if(!LIRSnodeinstack(lirinfo) && LIRSnodeinstack(lirinfo) && hirinfo->status==STATUS_HIR)
	{
		//Leave status as HIR and move to top of queue
                LIRSremovenode(stackHIR, hirinfo);
                LIRSpushtop(stackHIR, hirinfo);
		return;
	}//End HIR node not in LIRStack operation
	
	//Operation for NHIR node in LIRStack
	if(LIRSnodeinstack(lirinfo) && lirinfo->status==STATUS_NHIR)
	{	
		//Remove node from bottom of HIRStack, change to NHIR if present in LIRStack
                if(LIRSnodeinstack(stackHIR->bottom->kvnode->lirinfo->lirstackinfo))
		{
                        stackHIR->bottom->kvnode->lirinfo->lirstackinfo->status=STATUS_NHIR;
		}
		
		//Pop off bottom of HIRStack, and remove from key value store
		else
		{
                        LIRSpopbottom(stackHIR);
			kvArgs* kvargs = (kvArgs*)malloc(sizeof(kvArgs));
                        kvargs->cKey = lirnode->lirstackinfo->kvnode->cKey;
			del(kvargs); //!!!! DELETES ITEM FROM KVCACHE!!!!
		}
		
		//Promote node to top of LIRStack, demote bottom node and prune stack
                LIRSremovenode(stackLIR, lirinfo);
                LIRSpushtop(stackLIR, lirinfo);
		
                stackLIR->bottom->status=STATUS_HIR;
                stackLIR->bottom->kvnode->lirinfo->hirstackinfo->status=STATUS_HIR;
		
                LIRSpushtop(stackHIR, stackLIR->bottom->kvnode->lirinfo->hirstackinfo);
		LIRSprune(stackLIR);
		return;
	}//End operation for NHIR node in LIRStack
		
}//End LIRSRefresh


void LIRSprune(LIRStack* stack)
{
	//Pop bottom and remove from kv cache until a LIRNode is found
        while(stack->bottom->status!=STATUS_LIR)
	{
                LIRNodeStackInfo* oldbottom=stack->bottom;
                LIRSpopbottom(stack);
		kvArgs* kvargs = (kvArgs*)malloc(sizeof(kvArgs));
		kvargs->cKey = oldbottom->kvnode->cKey;
		del(kvargs); //!!!! DELETES ITEM FROM KVCACHE!!!!
	}
}

void LIRSremovenode(LIRStack* stack, LIRNodeStackInfo* node)
{	
	//Decrement stack size
	stack->currentsize--;

	if(node == stack->top)
	{
            stack->top = node->next;
	    node->next = node->prev = NULL;
	    return;
	}

        else if(node == stack->bottom)
	{
            LIRSpopbottom(stack);
	    return;
	}

	else
	{
	    node->prev->next = node->next;
	    node->next->prev = node->prev;
	    node->next = node->prev = NULL;
	    return;
	}
}

void LIRSpushtop(LIRStack* stack, LIRNodeStackInfo* node)
{
        if(stack->currentsize==0)
        {
            stack->top = stack->bottom = node;
            stack->currentsize++;
            return;
        }

        stack->top->prev = node;
        stack->top = node;

	stack->currentsize++;
	return;
}

//Pop bottom node off LIR Stack
void LIRSpopbottom(LIRStack* stack)
{

    LIRNodeStackInfo* prevbottom;

    if(stack->currentsize==0)
        return;

    if(stack->currentsize==1)
    {
        stack->top = stack->bottom = NULL;
        stack->currentsize--;
        return;
    }

    prevbottom=stack->bottom;
    stack->bottom = stack->bottom->prev;
    stack->bottom->next=NULL;
    prevbottom->prev=NULL;

    return;
}

void LIRSperfmeasure(bool hit)
{
	if(hit)
		hit++;
	else
		miss++;
}

bool LIRSnodeinstack(LIRNodeStackInfo* node)
{
	if(node->prev || node->next)
		return true;
	
	else
		return false;
}

void LIRSinitstack(LIRStack* stack, uint32_t Maxsize)
{
	stack->top=stack->bottom=NULL;
	stack->currentsize=0;
	stack->maxsize=Maxsize;
}

void LIRSinitnode(LIRNode* node)
{
	node = (LIRNode*)malloc(sizeof(LIRNode));
}
