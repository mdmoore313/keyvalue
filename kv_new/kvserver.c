#include <pthread.h>

#include "kvserver.h"

int main()
{
	int sockfd, clientfd;
	struct addrinfo *serv_info;
	struct sockaddr_storage client_addr;
	socklen_t sin_size;
	struct sigaction sa;
	int i, yes = 1;
        kv_node *arrkeys[100] = {NULL};
	keys = &arrkeys[0];

        //int count = 0;

#ifdef CLOCK_ALG
	kvclock = (Clock*) malloc(sizeof(Clock));
        clockinit(kvclock, (500));
#elif defined LIRS_ALG
	kvlirstack = (LIRStack*) malloc(sizeof(LIRStack));
	kvhirstack = (LIRStack*) malloc(sizeof(LIRStack));
        LIRSinitstack(kvlirstack, 500);
        LIRSinitstack(kvhirstack, 10);
#else
	kvLRU = (LRU*) malloc(sizeof(LRU));
        lruinit(kvLRU, 500);
#endif

	if(pthread_mutex_init(&cache_lock, NULL) != 0) {
		fprintf(stderr, "Cache mutex initialization failed.\n");
		exit(EXIT_FAILURE);
	}

	for(i = 0; i < ARRAYLEN; i++) {
		if(pthread_mutex_init(&kv_lock[i], NULL) != 0) {
			fprintf(stderr, "Mutex initialization failed.\n");
			exit(EXIT_FAILURE);
		}
	}

	serv_info = get_addr_list(PORT);

	if((sockfd = prep_socket(serv_info, yes)) == -1) {
		exit(EXIT_FAILURE);
	}

	if(listen(sockfd, 1) == -1) {
		perror("kvserver: listen");
		exit(EXIT_FAILURE);
	}

	sa.sa_handler = sigchld_handler;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;

	if(sigaction(SIGCHLD, &sa, NULL) == -1) {
		perror("kvserver: sigaction");
		exit(EXIT_FAILURE);
	}

	printf("kvserver: Ready for connections...\n");

	while(1) {
		sin_size = sizeof(client_addr);
		clientfd = accept(sockfd, (struct sockaddr*)&client_addr, &sin_size);

		if(clientfd == -1) {
			perror("kvserver: accept");
			continue;
		}

		kv_new_thread(handle_request, &clientfd);
	}

	close(sockfd);

	return 0;
}


void handle_request(void *targs)
{
	int recvd;
	unsigned char recv_buf[BUF_SIZE], send_buf[BUF_SIZE], *ptr;
	int sock = *((int*) targs);

	while((recvd = recv(sock, recv_buf, BUF_SIZE, 0)) != 0) {
		if(recvd > 0) {
			kv_args *kvargs = (kv_args*) malloc(sizeof(kv_args));
			kvargs->c_key = (char*) malloc(sizeof(char) * KEYLEN);
			ptr = deserialize_kv_message(recv_buf, kvargs);

			switch(kvargs->status) {
				case KV_PUT:
					put(kvargs);
					break;

				case KV_GET:
					get(kvargs);
					break;

				case KV_DEL:
                                        //del(kvargs);
					break;

				default:
					break;
			}

			ptr = serialize_kv_message(send_buf, kvargs);

			if(send(sock, send_buf, ptr - send_buf, 0) == -1) {
				perror("kvserver: send");
			}

			printf("free() in main.\n");
			free(kvargs->c_key);
			free(kvargs);
		}

		else {
			perror("kvserver: recv");
		}
	}

	close(sock);
}


void put(void *args)
{
	int i;
	char *key = ((kv_args*) args)->c_key;
	int value = ((kv_args*) args)->i_value;
	uint32_t index = hashkey(key) % ARRAYLEN;
	kv_node *new_node = (kv_node*) malloc(sizeof(kv_node));

	for(i = 0; (i < KEYLEN || *(key + i) != '\0'); i++) {
		new_node->c_key[i] = *(key + i);
	}

	printf("In put().\n");
	new_node->i_value = value;
	new_node->next = NULL;

#ifdef CLOCK_ALG
        new_node->clockinfo = (ClockNode*)malloc(sizeof(ClockNode));
#endif
#ifdef LIRS_ALG
	new_node->lirinfo = (LIRNode*) malloc(sizeof(LIRNode));
	new_node->lirinfo->lirstackinfo =
		(LIRNodeStackInfo*) malloc(sizeof(LIRNodeStackInfo));
	new_node->lirinfo->hirstackinfo =
		(LIRNodeStackInfo*) malloc(sizeof(LIRNodeStackInfo));
	new_node->lirinfo->lirstackinfo->kvnode = new_node;
	new_node->lirinfo->hirstackinfo->kvnode = new_node;
#elif !defined CLOCK_ALG
        new_node->kvLRUnode = (LRUNode*)malloc(sizeof(LRUNode));
        new_node->kvLRUnode->kvnode = new_node;
#endif

	kv_node **node;
	node = &keys[index];

	pthread_mutex_lock(&kv_lock[index]);

	while(*node) {
		if(strcmp((*node)->c_key, key) == 0) {
			(*node)->i_value = value;

			pthread_mutex_unlock(&kv_lock[index]);
			free(new_node);

			// Update cache
			pthread_mutex_lock(&cache_lock);
#ifdef CLOCK_ALG
			Clock_Refresh(kvclock, (*node)->clockinfo);
#elif defined LIRS_ALG
			LIRSrefresh(kvlirstack, kvhirstack, (*node)->lirinfo);
#else
			LRU_Add(kvLRU, new_node->kvLRUnode);
#endif
			pthread_mutex_unlock(&cache_lock);

			return;
		}

		node = &((*node)->next);
	}

	(*node) = new_node;

	pthread_mutex_unlock(&kv_lock[index]);

	((kv_args*) args)->status = KV_OK;

	// Update cache
	pthread_mutex_lock(&cache_lock);
#ifdef CLOCK_ALG
	Clock_Refresh(kvclock, (*node)->clockinfo);
#elif defined LIRS_ALG
	LIRSrefresh(kvlirstack, kvhirstack, (*node)->lirinfo);
#else
	LRU_Add(kvLRU, new_node->kvLRUnode);
#endif
	pthread_mutex_unlock(&cache_lock);
}


void get(void* args)
{
	char *key = ((kv_args*) args)->c_key;
	uint32_t index = hashkey(key) % ARRAYLEN;
	kv_node *node = keys[index];

        printf("In get() with %s", key);
	pthread_mutex_lock(&kv_lock[index]);

	while(node) {
		if (strcmp(node->c_key, key) == 0) {
			((kv_args*) args)->i_value = node->i_value;
			((kv_args*) args)->status = KV_OK;
			pthread_mutex_unlock(&kv_lock[index]);

			// Update cache
			pthread_mutex_lock(&cache_lock);
#ifdef CLOCK_ALG
                        //node->clockinfo->reference=false;
			Clock_Refresh(kvclock, node->clockinfo);
#elif defined LIRS_ALG
			LIRSrefresh(kvlirstack, kvhirstack, node->lirinfo);
#else
			LRU_Add(kvLRU, node->kvLRUnode);
#endif
			pthread_mutex_unlock(&cache_lock);
			return;
		}

		else
			node = node->next;
	}

	pthread_mutex_unlock(&kv_lock[index]);

/*	// Update cache
	pthread_mutex_lock(&cache_lock);
#ifdef CLOCK_ALG
	Clock_Refresh(kvclock, node->clockinfo);
#elif defined LIRS_ALG
	LIRSrefresh(kvlirstack, kvhirstack, node->lirinfo);
#else
	LRU_Add(kvLRU, node->kvLRUnode);
#endif */
	pthread_mutex_unlock(&cache_lock);

	((kv_args*) args)->status = KV_NOK;
}


void del(void* args)
{
	char *key = ((kv_args*) args)->c_key;

	//Grab hash and mod it w/ array length
	uint32_t index = hashkey(key) % ARRAYLEN;

	//Iterate through list and try to find the key
	kv_node **node = &keys[index];
	kv_node *prev_node = *node; //pointer to previous node in list

	printf("In del().\n");
	pthread_mutex_lock(&kv_lock[index]);

	while(*node) {
		// Node to be deleted has been found
		if (strcmp((*node)->c_key, key) == 0) {

			// Make sure list isn't broken if deletion node isn't last in the
			// list
			if ((*node)->next) {
				// This condition implies that the node to be deleted is the
				// first node on this chain
				if(*node == prev_node) {
					*node = (*node)->next;
                                        if(prev_node)
                                            free(prev_node);
				}

				else {
					kv_node *del_node = prev_node->next; //Node to be deleted
					prev_node->next = (*node)->next;

                                        if(del_node)
                                            free(del_node);
				}
			}

			else {
                                if(*node)
                                    free(*node);
			}

			((kv_args*) args)->status = KV_OK;
			pthread_mutex_unlock(&kv_lock[index]);
			return;
		}

		else {
			prev_node = *node;
			node = &((*node)->next);
		}
	}

	pthread_mutex_unlock(&kv_lock[index]);

	((kv_args*) args)->status = KV_NOK;
}


void sigchld_handler(int s)
{
	int saved_errno = errno;

	while(waitpid(-1, NULL, WNOHANG) > 0);

	errno = saved_errno;
}


struct addrinfo* get_addr_list(const char *port)
{
	int status;
	struct addrinfo hints, *serv;

	memset(&hints, 0, sizeof(hints));

	hints.ai_flags = AI_PASSIVE;
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;

	if((status = getaddrinfo(NULL, port, &hints, &serv)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(status));
		exit(EXIT_FAILURE);
	}

	return serv;
}


int prep_socket(struct addrinfo *list, int yes)
{
	struct addrinfo *p;
	int sock;

	for(p = list; p!= NULL; p = p->ai_next) {
		if((sock = socket(p->ai_family, p->ai_socktype,
						p->ai_protocol)) == -1) {
			perror("kvserver: socket:");
			continue;
		}

		if(setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &yes,
					sizeof(int)) == -1) {
			perror("kvserver: setsockopt");
			exit(EXIT_FAILURE);
		}

		if(bind(sock, p->ai_addr, p->ai_addrlen) == -1) {
			close(sock);
			perror("kvserver: bind");
			continue;
		}

		break;
	}

	freeaddrinfo(list);

	return sock;
}


#ifdef CLOCK_ALG

void clockinit(Clock* clock, uint32_t max)
{
	clock->bottom = NULL;
	clock->top = NULL;
	clock->max_length = max;
}


void Clock_Refresh(Clock* clock, ClockNode* clocknode)
{
	printf("In Clock_Refresh.\n");


        clocknode->reference = false;

	if (clock->current_length == 0)
	{
		clock->top = clocknode;
		clock->bottom = clocknode;
		return;
	}


	else if (clock->current_length < clock->max_length) //add bottom of list
	{
		clock->bottom->next = clocknode;
		clocknode->next = clock->top;
		clock->bottom = clocknode;
                clock->current_length++;
		return;
	}

	else
	{
		while (clock->current_position->reference == false)
                {
                        if(clock->current_position != clocknode)
                        {
                            clock->current_position->reference = true;
                        }
			clock->current_position = clock->current_position->next;
		}

		ClockNode* oldnode = clock->current_position;
		oldnode->previous->next = clocknode;
		clocknode->previous = oldnode->previous;
		clocknode->next = oldnode->next;
		oldnode->next->previous = clocknode;

		if (clock->top == oldnode)
		{
			clock->top = oldnode->next;
		}
		else if(clock->bottom == oldnode)
		{
			clock->bottom = oldnode->previous;
		}

                kv_args* kvargs = (kv_args*)malloc(sizeof(kv_args));
                kvargs->c_key = oldnode->kvnode->c_key;

                del(kvargs);
		return;
	}
}


#elif defined LIRS_ALG

void LIRSrefresh(LIRStack* stackLIR, LIRStack* stackHIR, LIRNode* lirnode)
{
	printf("In LIRSrefresh().\n");
	//Our kvNode's information regarding the LIRStack and HIRStack
	LIRNodeStackInfo *lirinfo = lirnode->lirstackinfo;
        LIRNodeStackInfo *hirinfo = lirnode->hirstackinfo;

	//If we are monitoring the performance of LIRS, increase hit count
	/*#ifdef LIRSPERFMEASURE*/
	/*//increase hit or miss count depending on status*/
	/*LIRSperfmeasure(((*/
		/*LIRSnodeinstack(lirinfo)||*/
		/*LIRSnodeinstack(hirinfo))&&lirinfo->status!=NHIR) true : false);*/
	/*#endif*/

	//Check if new node, a node that isn't in either stack
	if(!LIRSnodeinstack(lirinfo) && !LIRSnodeinstack(hirinfo))
	{
		//Add to LIRStack as LIRNode if LIRStack isn't full
		if(stackLIR->currentsize < stackLIR->maxsize)
		{
			lirinfo->status=STATUS_LIR;
            LIRSpushtop(stackLIR, lirinfo);
		}

		//LIRStack is full, add as HIR
		else
		{
			lirinfo->status=STATUS_HIR;
            LIRSpushtop(stackLIR, lirinfo);

			//Add to HIRStack as well
			hirinfo->status=STATUS_HIR;
            LIRSpushtop(stackHIR, hirinfo);

		}
		return;
	} //End new node operation

	//Operation for a LIRnode that already exists in StackLIR
	if(lirinfo->status==STATUS_LIR)
	{
		//If node is already on top, we are done
            if(stackLIR->top==lirinfo)
			return;

		//If node was on bottom, stack will need pruning
            else if(stackLIR->bottom==lirinfo)
            {
                //Else, remove and place on top
                LIRSremovenode(stackLIR, lirinfo);
                LIRSpushtop(stackLIR, lirinfo);
                LIRSprune(stackLIR);
                return;
            }

            //Else, remove and place on top
            else
            {
                LIRSremovenode(stackLIR, lirinfo);
                LIRSpushtop(stackLIR, lirinfo);
                return;
            }
	}//End LIRNode operation


	//Operation for HIR node in LIRStack
	if(LIRSnodeinstack(lirinfo)&&lirinfo->status==STATUS_HIR)
	{
		//Promote to top of LIRStack as LIRNode, remove from HIRStack if necessary
		lirinfo->status=STATUS_LIR;
        LIRSremovenode(stackLIR, lirinfo);
        LIRSpushtop(stackLIR, lirinfo);
		if(LIRSnodeinstack(hirinfo))
            LIRSremovenode(stackHIR, hirinfo);

		//Demote bottom node, move to stackHIR, and prune stackLIR
        stackLIR->bottom->status=STATUS_HIR;
        LIRSpushtop(stackHIR, hirinfo);
		LIRSprune(stackLIR);
		return;
	}//End HIR node in LIRStack operation

	//Operation for HIR node not in LIRStack
	if(!LIRSnodeinstack(lirinfo) && LIRSnodeinstack(lirinfo) && hirinfo->status==STATUS_HIR)
	{
		//Leave status as HIR and move to top of queue
        LIRSremovenode(stackHIR, hirinfo);
        LIRSpushtop(stackHIR, hirinfo);
		return;
	}//End HIR node not in LIRStack operation

	//Operation for NHIR node in LIRStack
	if(LIRSnodeinstack(lirinfo) && lirinfo->status==STATUS_NHIR)
	{
		//Remove node from bottom of HIRStack, change to NHIR if present in LIRStack
        if(LIRSnodeinstack(stackHIR->bottom->kvnode->lirinfo->lirstackinfo))
		{
	    	stackHIR->bottom->kvnode->lirinfo->lirstackinfo->status=STATUS_NHIR;
		}

		//Pop off bottom of HIRStack, and remove from key value store
		else
		{
            LIRSpopbottom(stackHIR);
			kv_args* kvargs = (kv_args*)malloc(sizeof(kv_args));
            kvargs->c_key = lirnode->lirstackinfo->kvnode->c_key;
			del(kvargs); //!!!! DELETES ITEM FROM KVCACHE!!!!
		}

		//Promote node to top of LIRStack, demote bottom node and prune stack
        LIRSremovenode(stackLIR, lirinfo);
        LIRSpushtop(stackLIR, lirinfo);

        stackLIR->bottom->status=STATUS_HIR;
        stackLIR->bottom->kvnode->lirinfo->hirstackinfo->status=STATUS_HIR;

        LIRSpushtop(stackHIR, stackLIR->bottom->kvnode->lirinfo->hirstackinfo);
		LIRSprune(stackLIR);
		return;
	}//End operation for NHIR node in LIRStack

}//End LIRSRefresh


void LIRSprune(LIRStack* stack)
{
	printf("In LIRSprune().\n");
	//Pop bottom and remove from kv cache until a LIRNode is found
    while(stack->bottom->status!=STATUS_LIR)
	{
        LIRNodeStackInfo* oldbottom=stack->bottom;
        LIRSpopbottom(stack);
		kv_args* kvargs = (kv_args*)malloc(sizeof(kv_args));
		kvargs->c_key = oldbottom->kvnode->c_key;
		del(kvargs); //!!!! DELETES ITEM FROM KVCACHE!!!!
	}
}

void LIRSremovenode(LIRStack* stack, LIRNodeStackInfo* node)
{
	printf("In LIRSremovenode().\n");
	//Decrement stack size

	if(node == stack->top)
	{
        stack->top = node->next;
	    node->next = node->prev = NULL;
            stack->currentsize--;
	    return;
	}

    else if(node == stack->bottom)
	{
        LIRSpopbottom(stack);
	    return;
        }

        else if(node->prev && node->next)
	{
	    node->prev->next = node->next;
		
		if(node->next != NULL)
			node->next->prev = node->prev;

	    node->next = node->prev = NULL;
            stack->currentsize--;
	    return;
	}
}

void LIRSpushtop(LIRStack* stack, LIRNodeStackInfo* node)
{
	printf("In LIRSpushtop.\n");
    if(stack->currentsize==0)
    {
        stack->top = stack->bottom = node;
        stack->currentsize++;
        return;
    }

    stack->top->prev = node;
    node->next = stack->top;
    stack->top = node;

	stack->currentsize++;
	return;
}

//Pop bottom node off LIR Stack
void LIRSpopbottom(LIRStack* stack)
{
	printf("In LIRSpopbottom().\n");
    LIRNodeStackInfo* prevbottom;

    if(stack->currentsize==0)
        return;

    if(stack->currentsize==1)
    {
        stack->top = stack->bottom = NULL;
        stack->currentsize--;
        return;
    }

    prevbottom=stack->bottom;
    stack->bottom = stack->bottom->prev;
    stack->bottom->next=NULL;
    prevbottom->prev=NULL;
    stack->currentsize--;
    return;
}

/*void LIRSperfmeasure(bool hit)*/
/*{*/
	/*if(hit)*/
		/*hit++;*/
	/*else*/
		/*miss++;*/
/*}*/

bool LIRSnodeinstack(LIRNodeStackInfo* node)
{
	printf("In LIRSnodeinstack().\n");
	if(node->prev || node->next)
		return true;

	else
		return false;
}

void LIRSinitstack(LIRStack* stack, uint32_t Maxsize)
{
	stack->top=stack->bottom=NULL;
	stack->currentsize=0;
	stack->maxsize=Maxsize;
}

void LIRSinitnode(LIRNode* node)
{
	node = (LIRNode*)malloc(sizeof(LIRNode));
}


#else

void lruinit(LRU* LRU, uint32_t max)
{
	LRU->bottom = NULL;
	LRU->top = NULL;
	LRU->max_length = max;
}

void LRU_Add(LRU* lru, LRUNode* LRUnode)
{
	printf("In LRU_Add().\n");
	if (LRUnode->next != NULL || LRUnode->previous != NULL)
	{
                LRU_Remove(lru, LRUnode);
                LRU_push(lru, LRUnode);
	}
	else if (lru->current_length < lru->max_length)
	{
                LRU_push(lru, LRUnode);
	}
	else
	{
                LRU_pop(lru);
                LRU_push(lru, LRUnode);
	}
}

void LRU_push(LRU* lru, LRUNode* LRUnode)
{
	printf("In LRU_push().\n");
	if (lru->current_length == 0)
	{
		lru->top = lru->bottom = LRUnode;
		lru->current_length++;
		return;
	}

	lru->top->previous = LRUnode;
        LRUnode->next = lru->top;
	lru->top = LRUnode;
	lru->current_length++;
	return;
}

void LRU_pop(LRU* lru)
{
	printf("In LRU_pop().\n");
	LRUNode* oldbottom = lru->bottom;
	if (lru->current_length == 0)
	{
		return;
	}
	if (lru->current_length == 1)
	{
		lru->top = lru->bottom = NULL;
		lru->current_length++;
		return;
	}
	oldbottom = lru->bottom;
	lru->bottom = lru->bottom->previous;
	lru->bottom->next = NULL;
	oldbottom->previous = NULL;
	return;
}

void LRU_Remove(LRU* lru, LRUNode* LRUnode)
{
	printf("In LRU_Remove().\n");
	lru->current_length--;
	if (LRUnode == lru->top)
	{
		lru->top = LRUnode->next;
		LRUnode->next = LRUnode->previous = NULL;
		return;
	}
	else if (LRUnode == lru->bottom)
	{
                LRU_pop(lru);
		return;
	}
        else if(LRUnode->previous && LRUnode->next)
	{
		LRUnode->previous->next = LRUnode->next;
		LRUnode->next->previous = LRUnode->previous;
		LRUnode->next = LRUnode->previous = NULL;
		return;
	}
}

#endif
